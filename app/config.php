<?php

//force local settings
ini_set('allow_url_fopen', 1 );
ini_set('default_charset', 'utf-8');
date_default_timezone_set('America/Los_Angeles');
error_reporting(E_ALL);

/**
 * MySQL database info
 *
 */
$db = array
(
  'host' => 'localhost',
  'name' => 'instatag',
  'user' => 'root',
  'pass' => 'root'
);

/**
 * connect to db
 *
 */
try
{
  $dbh = new PDO('mysql:host='. $db['host'] .';dbname='. $db['name'] , $db['user'], $db['pass']);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
  catch(PDOException $e)
{
  echo $e->getMessage();
}

