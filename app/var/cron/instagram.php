<?php

//get configs
require dirname(dirname(dirname(__FILE__))) .'/config.php';

//use vendor lib
require dirname(dirname(dirname(__FILE__))) .'/vendors/instagram.class.php';

/**
 * deal with empty location names
 *
 */
function null_names($d)
{	
  if(isset($d->location->name)) return (string)$d->location->name;
	
  return ''; 
}

function null_videos($d)
{ 
  if(isset($d->videos)) return (string)$d->videos->standard_resolution->url;
  
  return ''; 
}
/**
 * save to db
 *
 */
function save_item($d)
{
  global $dbh; //var_dump($d);
		
  $sql = array(
    'q' => "REPLACE INTO media SET
      gram_id              = ? ,
      created_time         = ? ,
      location             = ? ,
      standard_url         = ? ,
      hires_video_url      = ? ,
      user_profile_picture = ? ,
      link                 = ? ,
      caption              = ? ,
      username             = ? ,
      full_name            = ? ,
      user_id              = ? 
    ",
    'p' => array(
      $d->id,
      $d->created_time,
      null_names($d),
      $d->images->standard_resolution->url,
      null_videos($d),
      $d->user->profile_picture,
      $d->link,
      $d->caption->text,
      $d->user->username,
      $d->user->full_name,
      $d->user->id
  ));

  $p = $dbh->prepare($sql['q']);
  $p->execute($sql['p']);

  echo 'saved: '. date('Y-m-d h:i:s A',$d->created_time ) ."\n" ;
}

// Initialize class for public requests
$instagram = new Instagram('6f549e965021440085164a9699988f6e');

// Get pix for tag
$tag = 'futureancestors';
$p = $instagram->getTagMedia($tag);

//var_dump($p);

// Display results
foreach ($p->data as $data) {
  //echo "<pre>"; var_dump($data);echo "</pre>";

  //ignore media older than
  if( $data->created_time < strtotime("Oct 10, 2013") ) continue;

    save_item($data);

    //var_dump($data->id);
    //var_dump($data->created_time);
    //var_dump($data->location->name); //can be null
    //var_dump($data->images->standard_resolution->url); //612x612
    //var_dump($data->user->profile_picture) //profile pix

    //var_dump($data->link); //direct URL to instagram
    //var_dump($data->caption->text); //full string
    //var_dump($data->user->username); //username
    //var_dump($data->user->full_name); //users full name
    //var_dump($data->user->id) //user id

    //echo "<img src=\"{$data->images->standard_resolution->url}\">";
    //echo "<img src=\"{$data->user->profile_picture}\">";
    //echo "<hr><br/><br/>";
}


