<?php

//get configs
require '../../app/config.php';

//number
$n = isset($_REQUEST['n']) ? (int)$_REQUEST['n'] : 0;

Instatag::get(1,0,$n) ;


/**
 *  class.
 *
 *
 */
class Instatag {

	/**
	 * get function.
	 *
	 * @access public
	 * @static
	 * @param int $json (default: 0)
	 * @param int $offset (default: 0)
	 * @param int $num (default: 12)
	 * @return void
	 */

	static public function get($json = 0, $offset = 0, $num = 0)
	{		
		$sql = sprintf("SELECT * FROM media ORDER BY created_time DESC LIMIT %d, %d", $offset, $num) ;

		$data = self::get_all( $sql );

		if ( $json == 1)
		{
			header('Content-type: application/json');
			print json_encode( $data );
		}
		else
		{
			return $data;
		}
	}
	
	/**
	 * get_all function.
	 *
	 * @access public
	 * @static
	 * @param mixed $sql
	 * @return void
	 */
	static function get_all($sql)
	{
		global $dbh;
		$res= $dbh->query($sql);
		if ($res)
			return $res->fetchAll(PDO::FETCH_ASSOC);
	}

}
